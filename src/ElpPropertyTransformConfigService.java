
import java.net.UnknownHostException;
import java.util.Map;

//import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class ElpPropertyTransformConfigService implements java.io.Serializable{
	private static final long serialVersionUID = -640214225982178221L;
	private static Logger logger = LoggerFactory.getLogger(ElpPropertyTransformConfigService.class);
	private PropertyTransformService propertyTransformService = null;
	
    public static final String SYSTEMPARAMETER_CODE_ELP_PROPERTY_TRANSFORM = "system.elpmodel.entity.property.transform.supported.datetime.format";
    private static final String SPACK_MARK = "@";
    
    public static final String PROPERTY_TYPE_DATE = "date";
    public static final String PROPERTY_TYPE_DATETIME = "datetime";
    
    public ElpPropertyTransformConfigService(String mongoIP,int mongoPort, String mongoDB) {
    	if( null == mongoIP ) {
    		logger.error("ElpPropertyTransformConfigService:NULL mongoIP!");
    		return;
    	}

    	propertyTransformService = new PropertyTransformService(mongoIP,mongoPort,mongoDB);
    }
    
    public String[] getPropertyTransformPattern(String propertyType) {
    	PropertyTransformConfig transformConfig = this.propertyTransformService.findSystemParameter(SYSTEMPARAMETER_CODE_ELP_PROPERTY_TRANSFORM);
    	if( null == transformConfig ) {
    		logger.error("ElpPropertyTransformConfigService:get [SYSTEMPARAMETER_CODE_ELP_PROPERTY_TRANSFORM] failed!");
    		return null;
    	}
    	
    	Map<String, String> values = transformConfig.getValue();
    	for( Map.Entry<String, String> entry : values.entrySet() ) {
    		String propertyName = entry.getKey();
    		if( propertyName.equalsIgnoreCase(propertyType) ) {  /*get specific type*/
    			String patterns = entry.getValue();
    			//multiple patterns can be separated by '@'
//    			if( StringUtils.isBlank(patterns) ) {
//    				logger.error("patterns is blank,propertyType="+propertyType);
//    				return null;
//    			}
    			
    			String[] splitPatterns = patterns.split(SPACK_MARK);
    			if( null != splitPatterns ) {
    				return splitPatterns;
    			}
    		}
    	}
    	
    	logger.error("transformConfig.getValue() returns zero size map,this function will return null.");
    	return null;
    }
    
    private class PropertyTransformService implements java.io.Serializable{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 2184271050754053269L;
		private String mongoIP = null;
    	private int mongoPort = 0;
    	private String mongoDB = null;

    	private final String MONGO_COLLECTION_SYSTEMPARAMETER = "SystemParameter";
    	
    	public PropertyTransformService(String mongoIP, int mongoPort, String mongoDB) {
    		this.mongoIP = mongoIP;
    		this.mongoPort = mongoPort;
    		this.mongoDB = mongoDB;
    	}
    	
    	private DBCollection getCollection() {
    		MongoClient mongoClient = null;
			try {
				mongoClient = new MongoClient(mongoIP, mongoPort);
				DB database = mongoClient.getDB(mongoDB);
	            return database.getCollection(MONGO_COLLECTION_SYSTEMPARAMETER);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Get mongo collection failed!IP="+mongoIP+",port="+mongoPort+",collection="+MONGO_COLLECTION_SYSTEMPARAMETER,e);
			}
			
			return null;
    	}
    	
    	public PropertyTransformConfig findSystemParameter(String parameterCode) {
    		/*connect to mongo and get the collection*/
    		DBCollection collection = null;
    		collection = getCollection();
    		
    		try {
                if( null != collection ) {
                	/*make a query*/
                    DBObject query = new BasicDBObject();
                    query.put("code", ElpPropertyTransformConfigService.SYSTEMPARAMETER_CODE_ELP_PROPERTY_TRANSFORM);
                    DBObject result = collection.findOne(query);
                    if (null != result) {
                    	/*find the record via its code*/
                    	PropertyTransformConfig transformConfig = new PropertyTransformConfig();
                    	transformConfig.setCode((String)result.get("code"));
                    	transformConfig.setName((String)result.get("name"));
                    	transformConfig.setValue((Map<String, String>)result.get("value"));
                    	return transformConfig;
                    }
                }else {
                	logger.error("collection is null! return nothing!");
                } 
            } catch (Exception e) {
            	logger.error("Get property transform config failed!",e);
            }
    		
    		return null; /*default is nothing.*/
    	}
    }
    
    private class PropertyTransformConfig implements java.io.Serializable{
    	/**
		 * 
		 */
		private static final long serialVersionUID = -7531204883073479459L;
		//private String id;
        private String code;
        private String name;
        private Map<String, String> value;
        
        
//		public String getId() {
//			return id;
//		}
//		public void setId(String id) {
//			this.id = id;
//		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Map<String, String> getValue() {
			return value;
		}
		public void setValue(Map<String, String> value) {
			this.value = value;
		}
    }
}
